import Axios from 'axios'
import AuthService from './AuthService'
import { MessageBox, Message } from 'element-ui'

let root = 'http://smilenet.mx/smilenet/'
let api = root + 'api/'

let getConfig = function() {
  return {
    headers: {
      'AUTH': AuthService.getTokenFromLocalStorage()
    }
  }
}
Axios.interceptors.response.use(function(response) {
  return response
}, function(error) {
  switch (error.response.status) {
    case 401:
      MessageBox.alert('Vuelve a iniciar sesión para continuar', 'Sesión expirada', {
        confirmButtonText: 'Ir al login',
        callback: action => {
          location.href = '/admin'
        }
      })
      break
    case 400:
      Message({
        showClose: true,
        message: 'Ha ocurrido un error con la petición',
        type: 'error'
      })
      break
    case 500:
      Message({
        showClose: true,
        message: 'Ha ocurrido un error en el sistema',
        type: 'error'
      })
      break
    default:
      Message({
        showClose: true,
        message: 'Ha ocurrido un error inesperado',
        type: 'error'
      })
      break
  }
  return Promise.reject(error)
})

export default {
  get(url) {
    return Axios.get(api + url, getConfig())
  },
  post(url, body) {
    return Axios.post(api + url, body, getConfig())
  },
  put(url, body) {
    return Axios.put(api + url, body, getConfig())
  },
  delete(url) {
    return Axios.delete(api + url, getConfig())
  },
  getHeaders() {
    return getConfig().headers
  },
  getUrlApi() {
    return api
  },
  getUrlRoot() {
    return root
  }
}
