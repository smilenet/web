const TOKEN_NAME = 'smilenet_token'

export default {
  setTokenToLocalStorage(token) {
    window.localStorage.setItem(TOKEN_NAME, token)
  },
  getTokenFromLocalStorage() {
    return window.localStorage.getItem(TOKEN_NAME)
  },
  removeTokenFromLocalStorage() {
    window.localStorage.removeItem(TOKEN_NAME)
  },
  isAuthenticated() {
    return this.getTokenFromLocalStorage() !== null
  }
}
