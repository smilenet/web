import moment from 'moment'

export default {
  date(date) {
    return moment(date).format('DD/MM/YYYY')
  },
  dateParse(date) {
    return moment(date)
  }
}
