export const state = {
  pageTitle: 'App'
}

export const mutations = {
  setPageTitle(state, text) {
    state.pageTitle = text
  }
}
