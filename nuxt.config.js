module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Smilenet',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, user-scalable=no' },
      { hid: 'description', name: 'description', content: 'Smilenet' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLINT on save
    */
    extend(config, ctx) {
      if (ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },

    build: {
      vendor: [
        'element-ui',
        'axios',
        'moment',
        { src: 'moment/locale/es', ssr: false },
        { src: 'scrollreveal/dist/scrollreveal', ssr: false }
      ]
    },
  },

  css: [
    { src: 'assets/element-ui-theme/index.css', lang: 'css' },
    { src: 'swiper/dist/css/swiper.css', lang: 'css' },
    { src: 'normalize.css/normalize.css', lang: 'css' },
    { src: 'assets/index.scss', lang: 'scss' },
    { src: 'assets/transitions.css', lang: 'css' }
  ],

  plugins: [
    '~plugins/icons',
    '~plugins/element-ui',
    '~plugins/scroll-to',
    '~plugins/swiper',
    '~plugins/moment',
    { src: '~plugins/youtube-embed', ssr: false }
  ]
}
