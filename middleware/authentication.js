import AuthService from '~/services/AuthService'

export default function({ isServer, store, redirect }) {
  if (!isServer) {
    if (!AuthService.isAuthenticated()) {
      return redirect('/admin')
    }
  }
}
