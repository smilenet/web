import Vue from 'vue'
import Moment from 'moment'
Moment.locale('es')

Vue.use(Moment)
