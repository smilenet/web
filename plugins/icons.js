import Vue from 'vue'

import 'vue-awesome/icons/shopping-cart'
import 'vue-awesome/icons/gift'
import 'vue-awesome/icons/facebook'
import 'vue-awesome/icons/youtube'
import 'vue-awesome/icons/twitter'
import 'vue-awesome/icons/instagram'

import Icon from 'vue-awesome/components/Icon.vue'

Vue.component('icon', Icon)
